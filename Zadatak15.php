<?php

// Napisati PHP skriptu koja sabira prvih 20 neparnih prirodnih brojeva. Ispisati rezultat


$counter = 0;
$sledeci_neparni = 1;
$sum = 0;

while ($counter < 20) {

	$sum += $sledeci_neparni;

	$sledeci_neparni += 2;
    
    $counter++;
}  

echo "Zbir prvih 20 neparnih brojeva iznosi: $sum";