<?php

/* Napisati PHP skriptu koja na osnovu vrednosti promenljive $broj_bodova ispisuje konačnu ocenu. Postaviti da je broj bodova proizvoljan broj od 0 do 100. Ocena se određuje na sledeći način:
	
	a: Manje od 55 poena, ocena 5
	b: 55-64 poena, ocena 6
	c: 65-74 ocena 7
	d: 75-84 ocena 8
	e: 85-94 ocena 9
	f: 95+ ocena 10
*/

$broj_bodova = 68;

switch($broj_bodova) {
     
    case ($broj_bodova > 55 && $broj_bodova < 65):
        echo "Cestitamo dobili ste sesticu!";
        break;
    
    case ($broj_bodova > 65 && $broj_bodova < 75):
        echo "Cestitamo dobili ste sedmicu!";
        break;
    
    case ($broj_bodova > 75 && $broj_bodova < 85):
        echo "Cestitamo dobili ste osmicu!";
        break;
    
    case ($broj_bodova > 85 && $broj_bodova < 95):
        echo "Cestitamo dobili ste devetku!";
        break;
    
    case ($broj_bodova > 95 && $broj_bodova <= 100):
        echo "Cestitamo dobili ste desetku!";
        break;
    
    case ($broj_bodova < 55):
        echo "Zao nam je, pali ste ispit.";
        break;
    
    case ($broj_bodova > 100):
        echo "Niste dobro izracunali poene.";
    
}
