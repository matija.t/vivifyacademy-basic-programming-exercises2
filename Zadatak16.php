<?php

// Napisati PHP skriptu koja za date tri promenljive $a, $b, $c koje sadrže razlicite brojeve ispisuje koja promenljiva sadrži najveći broj, a koja promenljiva najmanji.

$a = 2;
$b = 4;
$c = 8;

if ($a >= $b && $a >= $c) {
    echo "Najveci broj je $a, u promenljivoj \$a. <br>";

} elseif ($b >= $a && $b >= $c) {
    echo "Najveci broj je $b, u promenljivoj \$b. <br>";

} else {
    echo "Najveci broj je $c, u promenljivoj \$c. <br>";
}

if ($a <= $b && $a <= $c) {
    echo "Najmanji broj je $a, u promenljivoj \$a.";

} elseif ($b <= $a && $b <= $c) {
    echo "Najmanji broj je $b, u promenljivoj \$b.";

} else {
    echo "Najmanji broj je $c, u promenljivoj \$c.";
}


