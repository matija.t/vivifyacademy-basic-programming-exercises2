<?php

// Napisati PHP skriptu koja ispisuje koji je godišnji kvartal u zavisnosti koji je mesec u godini. Koristiti if/else. Redni broj meseca u godini se može dobiti pomoću date("n").

$month = date("n");

if ($month >= 1 && $month <= 3) {

	echo "Prvi kvartal.";
}

elseif ($month >= 4 && $month <= 6) {

	echo "Drugi kvartal.";
}

elseif ($month >= 7 && $month <= 9) {

	echo "Treci kvartal.";
}

elseif ($month >= 10 && $month <= 12) {

	echo "Cetvrti kvartal.";
}
