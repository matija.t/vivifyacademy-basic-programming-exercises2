<?php

// Napisati PHP skriptu koja na osnovu vrednosti promenljive $n, koja sadrži broj, ispisuje “$n je neparan broj” ako je broj neparan, u suprotnom “$n je paran broj”.

$n = 5;

if ($n % 2 === 0) {
   echo '$n je paran broj';
} else {
    echo '$n je neparan broj';
}
