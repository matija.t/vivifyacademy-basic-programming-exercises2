<?php

// Napraviti promenljivu $a sa vrednošću 5 i promenljivu $b sa vrednošću 7. Ukoliko su obe promenljive tipa integer, ispisati poruku “Promenljive su tipa integer”, u suprotnom ispisati “Promenljive nisu tipa integer”. Koristiti if/else. Tip promenljive se dobija pomoću funkcije gettype, npr. tip promenljive $a se dobija pomoću gettype($a).

$a = 5;
$b = 7;

if (gettype($a) == 'integer' && gettype($b) == 'integer') {

	echo 'Promenljive su tipa integer';

} else {

	echo 'Promenljive nisu tipa integer';

}
