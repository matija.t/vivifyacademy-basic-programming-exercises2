<?php

// Napisati PHP skriptu koja sabira prvih 100 prirodnih brojeva. Ispisati rezultat

$start = 1;
$end = 100;

$sum = 0;

for ($i = $start; $i <= $end; $i++) {

    $sum += $i;
}

echo "Zbir brojeva od " . $start . " do " . $end . " iznosi " . $sum;
