<?php

// Napisati PHP skriptu koja za dati integer ispisuje poruku da li je broj jednocifren, dvocifren, ili trocifren.

$n = 555;
$absN = abs($n);

if ($absN < 10) {

    echo "Broj je jednocifren";

} elseif ($absN < 100) {

    echo "Broj je dvocifren";

} elseif ($absN < 1000) {

    echo "Broj je trocifren";
}
