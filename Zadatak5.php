<?php

// Napisati PHP skriptu koja za promenljive $a i $b koje sadrže brojeve ispisuje rezultat operacije koji je sadržan u promeljivoj $operacija (moze da bude 'saberi', 'oduzmi', 'pomnozi', 'podeli'). Koristiti switch


$a = 5;
$b = 10;

$operacija = '+';

switch ($operacija) {

    case "+":
    	echo "Rezultat sabiranja je: " . ($a+$b);
    	break;

    case "-":
   	 	echo "Rezultat oduzimanja je: " . ($a-$b);
    	break;
    
    case "/":
    	echo "Rezultat deljenja je: " . ($a/$b);
    	break;
    
    case "*":
    	echo "Rezultat mnozenja je: " . ($a*$b);
}