<?php

// Napisati PHP skriptu koja ispisuje prvih 10 neparnih prirodnih brojeva koristeći while petlju.

$counter = 0;
$sledeci_neparni = 1;

while ($counter < 10) {

	echo "$sledeci_neparni <br>";

	$sledeci_neparni += 2;

    $counter++;
}  

